<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:template match="/">
 <html>
  <title>PyWork XSLTView PetStore</title>
   <body bgcolor='white'>
    <p align='center'>
	 <big><b>Hi 
       <xsl:value-of select="result/username"/>
       welcome to the PETSTORE!</b></big></p>
    <p><b>Chooose a category</b></p>
    <p>
     <span>
	  <xsl:for-each select="result/categories/item">
       <a href="">
          <xsl:attribute name="href"><xsl:value-of select="pets"/></xsl:attribute>
          <xsl:value-of select="name"/>
       </a><br/>
      </xsl:for-each>
     </span>
    </p><a href='index.action'>Back</a>
   </body></html>
</xsl:template>
</xsl:stylesheet>

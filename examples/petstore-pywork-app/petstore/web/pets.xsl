<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:template match="/">
 <html>
  <title>PyWork XSLTView PetStore</title>
   <body bgcolor='white'>
	<p align='center'><big><b>You are viewing: <xsl:value-of select="result/category"/></b></big></p>
    <p>
     <span>
	  <xsl:for-each select="result/pets/item">
          <xsl:value-of select="text()"/>
          <br/>
      </xsl:for-each>
     </span>
    </p><a href='index.action'>Go Back</a>
   </body></html>
</xsl:template>
</xsl:stylesheet>

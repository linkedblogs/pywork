from pywork.action import Action

class BaseAction(Action):

	def __init__(self):
		super(BaseAction,self).__init__()

	def checkLogin(self):
		if self.name != "login.action" and "logged" not in self._session:
			self.redirectUri = "login.action"
			return 0
		else: return 1

	def _getUsername(self):
		try:
			return self._session[ "username" ]
		except KeyError:
			return None

	username = property(_getUsername)

class LoginAction(BaseAction):

	def __init__(self):
		super(LoginAction,self).__init__()
		self.__user = None
		self.__pass = None
		self.__sent = 0

	def __setUser(self,u):
		self.__user = u
	def __setPass(self,p):
		self.__pass = p
	def __getUser(self):
		return self.__user
	def __getPass(self):
		return self.__pass
	def __setSent(self,s):
		self.__sent = s

	def execute(self):
		if self.__sent:
			if (self.__user,self.__pass) == ("admin","admin"):
				self._session[ "logged" ] = 1
				self._session[ "username" ] = "ADMINISTRATOR"
				self.redirectUri = "index.action"
			else:
				return "error"

		else: return "success"
		
	input_user = property(__getUser,__setUser)
	input_pass = property(__getPass,__setPass)
	sent = property(None,__setSent)

class IndexAction(BaseAction):

	def execute(self):
		if not self.checkLogin: return
		return "success"

	def _getCategories(self):
		c = ( "Reptiles", "Birds", "Dogs", "Cats" )
		new = []
		for cat in c:
			new.append( { "name" : cat, "pets" : "pets.action?category=" + cat } )
		return new

	categories = property(_getCategories)

class PetsAction(BaseAction):

	def __init__(self):
		super(PetsAction,self).__init__()
		self.pets = []
		self.__category = None
		
	def execute(self):
		if not self.checkLogin: return

		if self.__category == "Reptiles":
			self.pets = ( "Iguana", "Boa", "Python" )
		elif self.__category == "Birds":
			self.pets = ( "Canary", "Dove", "Eagle" )
		elif self.__category == "Dogs":
			self.pets = ( "German Sheperd", "Golden", "Boxer" )
		elif self.__category == "Cats":
			self.pets = ( "Angora", "Siamese", "Street" )

		return "success"

	def __setCategory(self,c):
		self.__category = c
	def __getCategory(self):
		return self.__category

	category = property(__getCategory,__setCategory)

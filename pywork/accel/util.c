#include<stdio.h>
#include<string.h>
#include<stdarg.h>
#include<ctype.h>
#include<libgen.h>
#include<Python.h>

static PyObject *UtilError;
#define isxmlable(X) ( PyObject_HasAttr( X, PyString_FromString( "__dict__" ) ) || PyNumber_Check((X)) || PySequence_Check((X)) || PyMapping_Check((X))) 
#define XML_BUFSIZE 8192

void __append( char **buf, int *sz, int *fr,...)
{
	va_list ap;
	char *str;
	char *new;
	int n = 0;

  	va_start(ap, fr);
  	while( 1 ){
		str = va_arg(ap, char*);
		if( str == NULL ) break;

		n = strlen( str );
		while( n >= *fr ){
			new = (char *)PyMem_Malloc( *sz + XML_BUFSIZE );
			strcpy( new, *buf );
			PyMem_Free( *buf );
			*buf = new;
			*sz = *sz + XML_BUFSIZE;
			*fr = *fr + XML_BUFSIZE;
		}

		strcat( *buf, str );
		*fr = *fr - n;
	}

  	va_end (ap);
}


void
__recursiveObj2Xml( PyObject *obj, char **buf, int *sz, int *fr )
{
	PyObject *type, *__dict__, *keys, *key, *val, *str = NULL, *next;
	int n = 0, i;

	if( obj == Py_None ) return;

	if( ( type = PyObject_Type( obj ) ) != NULL ){
	 if( ( str = PyObject_Str( type ) ) != NULL ){

	//check if it has a class
	if( strncmp( PyString_AS_STRING( str ), "<class ", 7 ) == 0 ){

	/* here we get the object's properties */
	if( ( __dict__ = PyObject_Dir( obj ) ) != NULL ){
          n = PyList_GET_SIZE( __dict__ );
            for( i = 0; i < n; i++ ){
                key = PyList_GET_ITEM( __dict__, i );
                                                                                                                                                  
                /* if its not private */
                if( strncmp( PyString_AS_STRING( key ), "_", 1 ) && PyObject_HasAttr(obj, key) ){
					next = PyObject_GetAttr( obj, key );
					if( next != NULL ){
						if( isxmlable( next ) ){
							__append( buf, sz, fr, "<", PyString_AS_STRING( key ), ">", NULL );
	       	            	__recursiveObj2Xml( next, buf, sz, fr);
							__append( buf, sz, fr, "</", PyString_AS_STRING( key ), ">", NULL );
						}
						Py_DECREF( next );
					}
                }
            }
		}

		return;
	}
	Py_DECREF( str );
    }
	Py_DECREF( type );
	}

	/* complex types */

	/* list or tuples */
    if( PyList_Check(obj) ){
		n = PyList_GET_SIZE( obj );
		for( i = 0; i < n; i++ ){
			if( ( val = PyList_GET_ITEM( obj, i ) ) != NULL ){
				if( isxmlable( val ) ){
					__append( buf, sz, fr, "<item>", NULL );
               		__recursiveObj2Xml(val,buf,sz,fr);
					__append( buf, sz, fr, "</item>", NULL );
				}
			}
		}
	}
	else if( PyTuple_Check(obj) ){
		n = PyTuple_GET_SIZE( obj );
		for( i = 0; i < n; i++ ){
			if( ( val = PyTuple_GET_ITEM( obj, i ) ) != NULL ){
				if( isxmlable( val ) ){
					__append( buf, sz, fr, "<item>", NULL );
					__recursiveObj2Xml(val,buf,sz,fr);
					__append( buf, sz, fr, "</item>", NULL );
				}
			}
		}
	}
	/* dictionaries */
	else if( PyDict_Check(obj) ){
		if( ( keys = PyDict_Keys( obj ) ) != NULL ){
			n = PyList_GET_SIZE( keys );
			for( i = 0; i <n; i++ ){
				key = PyList_GET_ITEM( keys, i );
				
				if( ( next = PyDict_GetItem( obj, key ) ) != NULL ){
					if( isxmlable( next ) ){

						__append( buf, sz, fr, "<", PyString_AS_STRING( key ), ">", NULL );
	       	           	__recursiveObj2Xml( next, buf, sz, fr);
						__append( buf, sz, fr, "</", PyString_AS_STRING( key ), ">", NULL );

					}
				}
			}

			Py_DECREF( keys );
		}
	}
    else{
 		if( ( str = PyObject_Str( obj ) ) != NULL ){
			__append( buf, sz, fr, PyString_AS_STRING( str ), NULL );
		}
	}

}

static PyObject *
util_action2xml( PyObject *self, PyObject *args )
{
	PyObject *action;
	PyObject *xmlResult;
	char *buf = (char *)PyMem_Malloc( XML_BUFSIZE );
	int size = XML_BUFSIZE;
	int fr = size;

	if( !PyArg_ParseTuple( args, "O", &action ) ){
		PyErr_SetString( UtilError, "Error parsing function arguments" );
		return NULL;
	}

	//init string
	buf[ 0 ] = '\0';
	__append( &buf, &size, &fr, "<result>", NULL );
	__recursiveObj2Xml(action, &buf, &size, &fr);
	__append( &buf, &size, &fr, "</result>", NULL );
	xmlResult = PyString_FromString( buf );
	PyMem_Free( buf );

	return xmlResult;
}

/* module init */
static PyMethodDef UtilMethods[] = {
    {"action2xml",  util_action2xml, METH_VARARGS, "Convert a python object into an xml string"},
    {NULL, NULL, 0, NULL}        /* Sentinel */
};

void
initutil(void)
{
    PyObject *m, *d;

    m = Py_InitModule("util", UtilMethods);
    d = PyModule_GetDict(m);
	UtilError = PyErr_NewException( "util.error", NULL, NULL );
	PyDict_SetItemString( d, "error", UtilError );
}


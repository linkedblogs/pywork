# $Id: pywork.py,v 1.6 2004/06/22 08:17:18 julcicc Exp $
# vim:et:sts=4

import controller

# global controller
_gcontroller = controller.Controller()

def handler(req):
    return _gcontroller.processRequest( req )

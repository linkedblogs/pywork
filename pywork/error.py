# $Id: error.py,v 1.10 2004/06/21 10:33:20 julcicc Exp $
# vim:et:sts=4
import sys
import traceback
import log

try:
    from mod_python.apache import URI_PATH
except:
    URI_PATH = 0

trace = 1

def writeError(req, errorTitle, errorDescription):

    try:
        req.content_type = "text/html"
        req.write( "<html><head><title>PyWork Error</title></head><body><p><big><b>" )
        req.write( errorTitle )
        req.write( "</b></big></p>" )
        req.write( "<p>" )
        req.write( errorDescription )
        req.write( "</p>" )
        if trace:
            req.write( "<b>Trace is<br/></b>" )
            s = '\n<pre>\n'
            etype, evalue, etb = sys.exc_info()
            for e in traceback.format_exception(etype, evalue, etb):
                s = s + e + '\n'
            s = s + "</pre>\n"
                                                                                                                                           
            req.write(s)

    	req.write( "</body></html>" )
    except:
        #caught fatal error
        log.logFATAL( "Fatal error while writing error.. trace:" )
        etype, evalue, etb = sys.exc_info()
        for e in traceback.format_exception(etype, evalue, etb):
            log.logFATAL( e )



#general errors
def errorUnhandledException( req ):
    writeError( req, "Unable to process action", "Unhandled exception in '%s'" % req.parsed_uri[ URI_PATH ] )
    return 0

def errorInstance( req, id):
    writeError( req, "Unable to instantiate action", "Unable to create action instance for '%s'" % id )
    return 0

def errorActionNotFound( req, id):
    writeError( req, "Action not found", "Unable to find an entry in the action map '%s'" % id )
    return 0


def errorInvalidParameter(req, id, p = ""):
    writeError( req, "Invalid parameter '%s'" % p, "Unable to process request, invalid parameters were specified for action '%s'" % id )
    return 0

def errorUnauthorized(req, id):
    writeError( req, "Access Forbidden", "Unable to process request, invalid authentication tokens for action '%s'" % id )
    return 0


def errorInvalidView(req, id, view):
    writeError( req, "Invalid view", "'%s' returned an invalid view '%s'" % ( id, view ) )
    return 0

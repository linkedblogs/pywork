# $Id: util.py,v 1.10 2004/06/22 08:17:18 julcicc Exp $
# vim:et:sts=4

import os, stat, exception, log, types, cStringIO, copy, copy

try:
    from mod_python.apache import import_module
except:
    def import_module(name):
        mod = __import__(name)
        components = name.split('.')
        for comp in components[1:]:
            mod = getattr(mod, comp)
        return mod
    
pyworkpath = None

def searchpath( filename ):
    if globals()[ "pyworkpath" ]:
        filename = globals()[ "pyworkpath" ] + "/" + filename
    
    if os.path.exists( filename ) and os.path.isfile( filename ):
        return filename
    else:
        return ""

def getlastmodified( filename ):
    m = os.stat( filename )[ stat.ST_MTIME ]
    return m


class ClassResolverError(exception.BaseException): pass
        
def resolveobject( moduleName, name ):
    """
    This functions loads the object in the module specified
    """
    return getattr( import_module( moduleName ), name )

def reloadConfiguration( conf, viewconf, actionconf, req = None ):
    """
     This reloads the configuration of pywork, attention the order IS IMPORTANT
    """
    try:
        globals()[ "pyworkpath" ] = copy.copy(req.get_options()[ "PYWORK" ])
    except:
       if "PYWORK" in os.environ:
           globals()[ "pyworkpath" ] = os.environ[ "PYWORK" ]
                           
    if req is not None:
        d = {}
        for k, v in req.get_options().items():
            d[ copy.copy(k) ] = copy.copy(v)

        conf.set_options( d )

    conf.loadFromFile( "pywork.xml" )
    viewconf.loadFromFile( "pywork-views.xml" )
    actionconf.loadFromFile( "pywork-map.xml", conf, viewconf.availableViews )
    conf.loaded = 1

    # set log level
    log.LOG_DBG = conf.getconfint( "logDBG" )
    log.LOG_INFO = conf.getconfint( "logINFO" )
    log.LOG_ERROR = conf.getconfint( "logERROR" )
    log.LOG_WARN = conf.getconfint( "logWARN" )
    log.LOG_FATAL = conf.getconfint( "logFATAL" )

## now try to import the accel module

try:
    from accel.util import action2xml
except:
    def __recursiveObj2Xml(obj,sio):
       if obj is None: return

       if hasattr(obj,"__class__") and hasattr(obj.__class__,"__mro__"):

           for klass in obj.__class__.__mro__:
               for name,val in klass.__dict__.items():
                   if isinstance(val,property) and name[:1] != "_" and hasattr(obj, name):
                       sio.write( "<" )
                       sio.write( name )
                       sio.write( ">" )
                       __recursiveObj2Xml(getattr(obj,name),sio)
                       sio.write( "</" )
                       sio.write( name )
                       sio.write( ">" )

       try:

           for name,val in obj.__dict__.items():
               if name[:1] != "_":
                   sio.write( "<" )
                   sio.write( name )
                   sio.write( ">" )
                   __recursiveObj2Xml(val,sio)
                   sio.write( "</" )
                   sio.write( name )
                   sio.write( ">" )
                                                                                   
           return
       except:
           # important part for complex types
           if isinstance(obj,types.ListType) or isinstance(obj,types.TupleType):
               for val in obj:
                   sio.write( "<item>" )
                   __recursiveObj2Xml(val,sio)
                   sio.write( "</item>" )
           elif isinstance(obj,types.DictType) or isinstance(obj,types.DictionaryType):
               for key,val in obj.items():
                   sio.write( "<" )
                   sio.write( key )
                   sio.write( ">" )
                   __recursiveObj2Xml(val,sio)
                   sio.write( "</" )
                   sio.write( key )
                   sio.write( ">" )
           else:
               sio.write( obj )

    def action2xml(action):
       sio = cStringIO.StringIO()
       sio.write( "<result>" )
       __recursiveObj2Xml(action,sio) 
       sio.write( "</result>" )
       s = sio.getvalue()
       sio.close()
       return s




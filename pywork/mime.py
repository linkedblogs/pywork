# $Id: mime.py,v 1.1 2004/05/13 21:33:26 julcicc Exp $
# vim:et:sts=4

_MIME = {}

def parse_types(f):
    r = {}

    for l in f.readlines():
        l = l.strip() 
        if not l: continue 
        if l[0] == "#": continue

        cols = l.split()
        try:
            type = cols[ 0 ]
            xts = cols[ 1 ].split()
            for xt in xts:
                r[ xt ] = type

        except IndexError: continue

    return r
        
def init_cache():
    global _MIME
    if not _MIME:
        try:
            f = file( "/etc/mime.types", "r" )
            _MIME = parse_types(f)
        except OSError:
            print "Warning couldnt load /etc/mime.types"
            _MIME = { "html" : "text/html",
                      "xml" : "text/xml",
                      "gif" : "image/gif",
                      "jpg" : "image/jpeg" }


def get_type( fpath ):
    init_cache()
    xt = fpath.lower().split(".")[-1:][0]
    try:
        return _MIME[ xt ]
    except KeyError:
        return "text/plain"


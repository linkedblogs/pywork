# $Id: view.py,v 1.12 2004/06/21 10:33:20 julcicc Exp $
# vim:et:sts=4

import log, util, descriptor, exception, types, mime
import threading
import xml.parsers.expat

class ViewHandlerConfigurationError(exception.BaseException): pass

try: from psyco.classes import *
except: pass
    
class ViewHandler(object):

    def __init__(self):
        self.descriptor = None
        self.action = None
        self.viewFile = None
        self.context = None

    def viewCacheEnabled(self):
        return self.context.gconf.getconfint( "VIEW_CACHE_ENABLED" )

    def display(self,request):
        self.context.writeError(request,"ViewHandler not properly subclassed", "This view handler: " + self.___class__.__name__ + " does not overrid the display method" )

registeredExtensions = {}

def register_extension( extension, name ):
    if isinstance( extension, types.ListType ) or isinstance( extension, types.TupleType ):
        for xt in extension:
            registeredExtensions[ xt ] = name
    else:
        registeredExtensions[ extension ] = name

def resolve_viewfile( filename ):
    try:
        for ext,view in registeredExtensions.items():
            l = len(ext)
            if filename[-l:] == ext: return view
    except:
        pass

    return None
        

"""
    PythonViewHandler
"""
class PythonViewHandler(ViewHandler):
    
    def display(self,request):
        """
         The PythonHandler just locates the module/function and calls it with the action, then outputs the returned string
        """
        
        # get the module
        # and object
        # expected in "module<.submodule>*.function" format 

        i = self.viewFile.rfind( "." )
        if i >= 0:
            j = i + 1
            obj = util.resolveobject( self.viewFile[:i], self.viewFile[j:] )
            request.write( obj( self.action ) )
        else: raise TypeError( "Invalid function class" )


"""
  PassViewHandler is now the default view handler and delivers static res
"""
class PassViewHandler(ViewHandler):
    
    def display(self,req):
        """
         The PassViewHandler will determine the mime type based on the extension
        """
        req.content_type = mime.get_type( self.viewFile )        
        req.write( file( self.viewFile, "r" ).read() )


### ZPT view handler
"""
 Wrapper class for ZPT running outside Zope
"""
_zptInstalled = 0
try:
    from ZopePageTemplates import PageTemplate
    _zptInstalled = 1
except:
    log.logERROR( "Unable to create ZPT view handler" )

if _zptInstalled:
    class ZPTExternalTemplate(PageTemplate):
        def __call__(self, context={}, *args):
            if not context.has_key('args'):
                context['args'] = args
            return self.pt_render(extra_context=context)

    """
        PyWork ZPT view handler class
    """
    import time
    class ZPTViewHandler(ViewHandler):
        __cache__ = {}
        createLK = threading.Lock()
        writeLK = threading.Lock()

        
        def display(self,request):

            request.headers_out[ "Expires" ] = "Mon, 26 Jul 1997 05:00:00 GMT"
            request.headers_out[ "Last-Modified" ] = time.strftime( "%a, %d %b %Y %H:%M:%S GMT", time.gmtime() )
            request.headers_out[ "Cache-Control" ] = "no-store, no-cache, must-revalidate, post-check=0, pre-check=0"
            request.headers_out[ "Pragma" ] = "no-cache"

            #here we should implement some kind of pool for cached ZPT's

            zpt = None
            isShared = False
            cache = self.viewCacheEnabled()
            if cache:
                try:
                    self.createLK.acquire()
                    zpt = ZPTViewHandler.__cache__[ self.viewFile ]
                    isShared = True
                    self.createLK.release()
                except KeyError: pass

            if zpt is None:
                try:
                    zpt = ZPTExternalTemplate()
                    zpt.id = self.viewFile
                    zpt.write( file( self.viewFile, "r" ).read() )

                    if cache:
                        ZPTViewHandler.__cache__[ self.viewFile ] = zpt
                        isShared = True

                finally:
                    if cache: self.createLK.release()

            try:
                if isShared: self.writeLK.acquire()
                result = zpt( context = { "here" : self.action } )
            finally:
                if isShared: self.writeLK.release()

            request.write( result )
            
            
### XSLT view handler
"""
 This class gets the action convert's it in to a proper XML file an aplies the xslt stylesheet specified
 it then writes the result to request.write. We use libxml2, libxslt (from xmlsoft.org GNOME's standard xml libraries) python bindings to process XSLT files
"""
_xsltInstalled = 0

try:
    import libxml2, libxslt
    _xsltInstalled = 1
except:
    log.logERROR( "Unable to create XSLT view handler" )
    _xsltInstalled = 0

if _xsltInstalled:

    class XSLTViewHandler(ViewHandler):

        def display(self,request):

            style = libxslt.parseStylesheetFile(self.viewFile)
            if not style:
                request.write( "Error parsing xsl file %s" % self.viewFile )
            else:
                sXML = util.action2xml( self.action )
                doc = libxml2.parseMemory( sXML, len(sXML ) )
                result = style.applyStylesheet(doc, None)
                request.write( style.saveResultToString(result) )
                style.freeStylesheet()
                doc.freeDoc()
                result.freeDoc()
            
class ViewHandlerMapXMLLoader(object):

    def __init__(self):
        self.names = []
        self.map = {}
        self.instances = {}
        self.__parser = xml.parsers.expat.ParserCreate()
        self.__parser.StartElementHandler = self.start_element
        self.__clean()

    def initNames(self):
        self.names.append( "PASS" )
        self.names.append( "PYTHON" )
        if _xsltInstalled: self.names.append( "XSLT" )
        if _zptInstalled: self.names.append( "ZPT" )

    def start_element(self, name, attrs):
        ##convert attrs to str to eliminate unicode
        for k, v in attrs.items():
            attrs[ str(k) ] = str(v)

        
        if name == "view-handler":

            if attrs.has_key( "name" ) and attrs.has_key( "class" ) and attrs.has_key( "module" ):  
                name = attrs[ "name" ]
                className = attrs[ "class" ]
                moduleName = attrs[ "module" ]

            
                if name != "" and className != "" and moduleName != "":
                    
                    if name not in self.names:
                        self.names.append( name )
                        vd = descriptor.ViewHandlerDescriptor()
                        vd.name = name
                        vd.className = className
                        vd.moduleName = moduleName

    
                        # try to load class
                        # it will throw an error if something's bad
                        vd.loadClass()
                        self.map[ name ] = vd
                        self.instances[ name ] = vd.instance()
 
                    else:
                        raise ViewHandlerConfigurationError( "View handler name : %s was specified more than once" % name )
    
                else:
                    raise ViewHandlerConfigurationError( "One mandatory attribute is missing please check name, module and class" )

    def parseFile(self, fileName):
        self.__clean()
        f = file( fileName, "r" )
        self.__parser.ParseFile( f )
        f.close()

    def __clean(self):
        self.names = []
        self.map = {}
        self.instances = {}
        self.initNames()

# attention views instances are cacheable
class ViewHandlerMap(object):

    def __init__(self):
        self.__map = {}
        self.__instances = {}

        self.__lastMtime = 0
        self.availableViews = []

        #register defaults
        self.__registerDefaults()
        
    def __registerDefaults(self):
        v = self.__getPassDescriptor()
        self.__map[ "PASS" ] = v
        self.__instances[ "PASS" ] = v.instance()

        v = self.__getPythonDescriptor()
        self.__map[ "PYTHON" ] = v
        self.__instances[ "PYTHON" ] = v.instance()
        register_extension( ".py", "PYTHON" )

        xv = self.__getXSLTDescriptor()
        if xv != None:
            self.__map[ "XSLT" ] = xv
            self.__instances[ "XSLT" ] = xv.instance()
            register_extension( ( ".xsl", ".xslt" ) , "XSLT" )

        zv = self.__getZPTDescriptor()
        if zv != None:
            self.__map[ "ZPT" ] = zv
            self.__instances[ "ZPT" ] = zv.instance()
            register_extension( ( ".zpt", ".zpt.html", ".zpt.xml" ), "ZPT" )

    def __getPythonDescriptor(self):
        v = descriptor.ViewHandlerDescriptor()
        v.name = "PYTHON"
        v.className = "PythonViewHandler"
        v.moduleName = "pywork.view"
        v.classObject = PythonViewHandler
        return v

    def __getPassDescriptor(self):
        v = descriptor.ViewHandlerDescriptor()
        v.name = "PASS"
        v.className = "PassViewHandler"
        v.moduleName = "pywork.view"
        v.classObject = PassViewHandler
        return v

    def __getXSLTDescriptor(self):
        if _xsltInstalled:
            v = descriptor.ViewHandlerDescriptor()
            v.name = "XSLT"
            v.className = "XSLTViewHandler"
            v.moduleName = "pywork.view"
            v.classObject = XSLTViewHandler
            return v
        else:
            return None

        
    def __getZPTDescriptor(self):
        
        if _zptInstalled:
            v = descriptor.ViewHandlerDescriptor()
            v.name = "ZPT"
            v.className = "ZPTViewHandler"
            v.moduleName = "pywork.view"
            v.classObject= ZPTViewHandler
            return v
        else:
            return None

    def loadFromFile(self, file):
        path = util.searchpath( file )

        if path == "":
            log.logERROR( "View definition file: " + file + " not found, try setting the PYWORK env var, continuing" )
            return

        m = util.getlastmodified( path )

        # only load if something's changed
        if m != self.__lastMtime:
            a = ViewHandlerMapXMLLoader()
            a.parseFile( path )
            self.__map = a.map
            self.__instances = a.instances
            self.availableViews = a.names
            self.__lastMtime = m
            self.__registerDefaults()

    def getViewHandlerDescriptor(self, id):

        try:
            return self.__map[ id ]
        except:
            return None

    def getViewHandlerInstance(self, id):

        try:
            return self.__instances[ id ]
        except:
            return None


# $Id: descriptor.py,v 1.9 2004/03/30 10:52:55 julcicc Exp $
# vim:et:sts=4
import util, sys

class GeneralDescriptor(object):
            
    def __init__(self):
        self.classObject = None
        self.className = None
        self.moduleName = None
        self.name = None

    def loadClass(self,reimport=0):
        if not self.classObject:
            self.classObject = util.resolveobject( self.moduleName, self.className )
        elif reimport:
            try:
                del sys.modules[ self.moduleName ]
            except KeyError: pass
            self.classObject = util.resolveobject( self.moduleName, self.className )

    def instance(self):
        return self.classObject()

    def __str__(self):
        return "name = %s, moduleName = %s, className = %s" % ( self.name, self.moduleName, self.name )
    
### XXX hack to put session available before __init__
def actionCreator(cls,session):
    """
    Creates an action with the specified session
    This serves to initialize attributes prior to __init__
    """
    i = object.__new__(cls)
    i._session = session
    i.__init__()
    return i

class ActionDescriptor(GeneralDescriptor):
    
    def __init__(self):
        self.context = None
        self.views = {}
        super(ActionDescriptor,self).__init__()
        
    def instance(self,session,reload=0):
        global actionCreator

        if reload:
            self.classObject = None
            self.loadClass(1)

        obj = actionCreator(self.classObject,session)
        obj._descriptor = self
        return obj

    def __str__(self):
        return super(ActionDescriptor,self).__str__() + ( ", views = %s" % self.views )

class ViewDescriptor(object):

    def __init__(self):
        self.name = None
        self.viewHandlerName = "PYTHON"
        self.viewFile = None
        self.viewContentType = "text/html"

    def __str__(self):
        return "name = %s, viewHandlerName = %s, viewFile = %s, viewContentType = %s" % ( self.name, self.viewHandlerName, self.viewFile, self.viewContentType )

class ViewHandlerDescriptor(GeneralDescriptor):

    def __init__(self):
        super(ViewHandlerDescriptor,self).__init__()
        
    def instance(self):
        obj = super(ViewHandlerDescriptor,self).instance()
        obj.descriptor = self
        return obj


# $Id: log.py,v 1.7 2004/06/21 10:33:20 julcicc Exp $
# vim:et:sts=4

import time, os

# debugLevels
LOG_DBG = 1
LOG_INFO = 1
LOG_ERROR = 1
LOG_WARN = 1
LOG_FATAL = 1

try:
    __logfile = file( "/tmp/pywork.log", "a" )
except:
    import sys
    __logfile = sys.stderr

# TODO: proper file handling

def log( level, str ):
    date = time.strftime("%a, %d %b %Y %H:%M:%S +0000", time.gmtime())
    pid = os.getpid()
    __logfile.write("%(date)s -- %(level)s [%(pid)s]-- %(str)s\n" % vars())
    __logfile.flush()

def logDBG( str ):
    if LOG_DBG: log( "debug", str )

def logINFO( str ):
    if LOG_INFO: log( "info", str )

def logERROR( str ):
    if LOG_ERROR: log( "error", str )

def logWARN( str ):
    if LOG_WARN: log( "warn", str )

def logFATAL( str ):
    if LOG_FATAL: log( "fatal", str )


# $Id: controller.py,v 1.31 2004/06/24 08:56:41 julcicc Exp $
# vim:et:sts=4

import action, conf, log, view, error, util, re, time, copy

try:
    from mod_python.util import FieldStorage as HTTPParser
    from mod_python.apache import URI_PATH, REMOTE_NOLOOKUP
except:
    log.logERROR( "Could not import apache" )
    URI_PATH = 0
    REMOTE_NOLOOKUP = 0
    class HTTPParser(object):
        def __init__(self,r,keep_blank_values):
            self.list = []

try: from psyco.classes import *
except: pass

# global definitions
_globalConfiguration = conf.Configuration()
_globalViewHandlerMap = view.ViewHandlerMap()
_globalActionMap = action.ActionMap()

class Controller(object):

    def __init__(self):
        self.gconf = _globalConfiguration
        self.gview = _globalViewHandlerMap
        self.gaction = _globalActionMap
        self.__regex = re.compile( "([^}]*)\[([^}]*)\]" )

    def getconf( self, property ):
        return self.gconf.getconf( property )


    def _go( self, s, l ):
        x = self.__regex.match( s )
        if not x: 
            l.append( s )
            return
        else:
            g = x.groups()
            l.append( g[ 1 ] )
            self._go( g[ 0 ], l )

    def __processInput(self, req, actionInstance):
        """
        This method should return a dictionary with all the properties of the request
        TODO: process FILES and Arrays
        """
        # process input, if any
        log.logDBG( "Headers are %s" % str(req.headers_in) )
        log.logDBG( "Process input 1" )
        fs = HTTPParser(req, keep_blank_values=1)
        log.logDBG( "Process input 2" )
        req.form = fs
        log.logDBG( "Process input 3" )

        args = {}

        # step through fields
        log.logDBG( "Process input 4" )
        for field in fs.list:
        
            log.logDBG( "Process input in %s" % field.name )
            if field.filename:
                # this is a file
                val = action.fileupload(field.filename,field.file)
            else:
                # this is a simple string
                val = field.value


            ### avoid strange seg fault
            k = str(field.name)
            v = val
    
            actionInstance._actionParameters[ k ] = v

            l = []
            self._go( k, l )
            if len(l) == 1:
                args[ k ] = v
            else:
                l.reverse()
                xv = args
        
                for xx in l:
        
                    if xx == "":
                        k = []
                        if not isinstance( xv, list ):
                            last[ lastx ] = k
                            xv = k
                        last = xv
                    else:
        
                        n = {}
                        if isinstance( xv, list ):
                            n2 = { xx: n }
                            last = xv
                            xv.append( n2 )
                            xv = n
                        else:
                            try:
                                last = xv
                                xv = xv[ xx ]
                            except KeyError:
                                xv[ xx ] = n
                                last = xv
                                xv = n
        
                    lastx = xx
        
                if lastx == "":
                    last.append( v )
                else:
                    last[ lastx ] = v

        log.logDBG( "Phisycal parameters are: %s" % str(args) )

        return args

    def __setActionProperties( self, action, request ):
        """
            Action properties are setted as slots so carefull
        """
        bind = None
        actionEvent = None

        #for pName,pValue in self.__processInput( request, action ).items():
        v = self.__processInput( request, action ).items()
        for pName,pValue in v:
            log.logDBG( "[%s] Processing parameter %s with %s" % (action._name,pName,str(pValue)) )
            if pName[:1] != "_":

                try:
                    log.logDBG( "[%s] Trying to set parametter %s with %s" % (action._name,pName,str(pValue)) )
                    if isinstance( getattr( action.__class__, pName ), property ): 
                        setattr( action, pName, pValue ) 
                    elif action._variableFormProperties:
                        action._form[ pName ] = pValue
                    else: raise TypeError

                except AttributeError:

                    if action._variableFormProperties:
                        action._form[ pName ] = pValue
                    else:
                        log.logERROR( "[%s] Called with invalid parameter %s" % (action._name,pName) )
                        raise AttributeError, pName

            elif pValue != "": #magic protected params
                log.logDBG( "[%s] Looking up binding for %s in %s" % (action._name,pName,str(action._eventBindings) ) )
                try:
                    bind = action._eventBindings[ pName ]
                    if bind:
                        action.setCurrentEventParamater( pName )
                        log.logDBG( "[%s] Binded parameter %s to %s" % (action._name,pName,bind.im_func.func_name) )
                    else:
                        bind = action.execute
                        log.logERROR( "[%s] Binded parameter %s to None, using execute!" % (action._name,pName) )
                except KeyError:
                    log.logINFO( "[%s] Got parameter %s but no binding found" % (action._name,pName) )

        log.logDBG( "Returning from setaction par" )
        return bind
        
    def __getSession( self, req ):
        return self.gconf.sessionCall( req )

    def processRequest(self, req):

        try:
        
            log.logDBG( "(%s) Enter process request" % req.unparsed_uri )

            startReq = time.clock()

            """
                This function process requests, and lets mod_python handle exceptions
            """
            if self.gconf.reload: util.reloadConfiguration(self.gconf, self.gview, self.gaction, req)
    
            id = req.parsed_uri[URI_PATH]

            if self.gconf.debug:
                if id == "0" or not id: #trap error
                    ## somethings wrong
                    log.logERROR( "Strange uri_path detected, headers are %s" % str(req.headers_in) )

            log.logDBG( "Calling to get %s" % id )
            ad = self.gaction.getActionDescriptor( id )
            if not ad:
                log.logERROR( "[%s] Action descriptor not found" % id )
                return error.errorActionNotFound(req,id)

            try:
                ## now the session is available prior to __init__
                ## XXX there should be a way todo this more clean
                sess = self.__getSession(req)
                if self.gconf.debug: action = ad.instance(sess,1)
                else: action = ad.instance(sess)
 
            except:
                log.logERROR( "[%s] Unable to create action instance" % id )
                return error.errorInstance( req, id )

            v = self._doAction(action,ad,id,req)

            endReq = time.clock()
            if self.gconf.timing:
                totalReq = round(endReq-startReq, 3)
                log.logDBG( "[%s] Process request took: %s" % (id,totalReq) )

            log.logDBG( "(%s) End process request" % req.unparsed_uri )
            return v

        except:
            return error.errorUnhandledException( req )

    def _doAction(self,action,ad,id,req,params=1):
       try:
            action._context = self
            action._remoteAddress = copy.copy(req.get_remote_host(REMOTE_NOLOOKUP))
    
            try:
                log.logDBG( "[%s] Trying to detect locale" % id )
                locale = req.headers_in[ "Accept-Language" ]
                action._locale = locale
                log.logDBG( "[%s] Ok got locale: %s" %(id,locale) )
            except: pass
    
       
            action._name = id
    
            if params:
                try:
                    bind = self.__setActionProperties(action,req)
                    log.logDBG( "got something" )
                    if not bind: bind = action.execute
                    log.logDBG( "got something 2" )
                except AttributeError, d:
                    log.logDBG( "got something 3" )
                    return error.errorInvalidParameter(req,id,d)
            else:
                bind = action.execute
            
            # do session stuff
            log.logDBG( "got something 4" )
            sess = action._session
            log.logDBG( "got something 5" )

            # now process request
            t1 = time.clock()
            ### AUTH ###
            log.logDBG( "got something 5 a" )
            authRet = action.auth()
            log.logDBG( "got something 5 b" )
            t2 = time.clock()
            log.logDBG( "got something 6" )
            if self.gconf.timing:
                t = round(t2-t1, 3)
                log.logDBG( "[%s] Auth call took: %s" % (id,t) )

            if not authRet:
                if sess is not None:
                    sess.unlock()
                    sess.save()

                if action._redirectUri:
                    req.headers_out[ "Location" ] = action._redirectUri
                    return 301
                else: return error.errorUnauthorized(req,id)
    
            t1 = time.clock()
            log.logDBG( "[%s] Calling preExecute" % id )
            ### PRE EXECUTE ###
            view = action.preExecute()
            log.logDBG( "[%s] PreExecute returned: %s" % (id,view) )
            t2 = time.clock()
            if self.gconf.timing:
                t = round(t2-t1, 3)
                log.logDBG( "[%s] PreExecute took: %s" % (id,t) )

            if view or action._redirectUri:
                if sess is not None:
                    sess.unlock()
                    sess.save()
                return self.try_handle_view(req,ad,action,view)

            t1 = time.clock()
            log.logDBG( "[%s] Calling %s" % (id,bind.im_func.func_name) )
            ### EXECUTE ###
            view = bind()
            log.logDBG( "[%s] %s returned: %s" % (id,bind.im_func.func_name,view) )
            t2 = time.clock()
            if self.gconf.timing:
                t = round(t2-t1, 3)
                log.logDBG( "[%s] %s call took: %s" % (id,bind.im_func.func_name,t) )

            log.logDBG( "[%s] Calling postExecute" % id )

            ### POST EXECUTE ###
            action.postExecute()
            # release any session locks
            # mod_python sadly locks
            if sess is not None:
                sess.unlock()
                sess.save()

            t1 = time.clock()
            log.logDBG( "[%s] Processing view" % id )
            vresult = self.try_handle_view(req,ad,action,view)
            t2 = time.clock()
            if self.gconf.timing:
                t = round(t2-t1, 3)
                log.logDBG( "[%s] View processing took: %s" % (id,t) )

            return vresult

       except Exception, ex:
            
            l = str(self.gconf.exceptionHandlers.get( ex.__class__.__name__, 0 ))
            if l:
                try:
                    ad = self.gaction.getActionDescriptor(l)
                    if ad:

                        sess = self.__getSession(req)
                        if self.gconf.debug: action = ad.instance(sess,1)
                        else: action = ad.instance(sess)
    
                        return self._doAction(action,ad,l,req,params=False)
                    else:
                        return error.errorUnhandledException( req )
    
                except:
                    return error.errorUnhandledException( req )

            else:
                return error.errorUnhandledException( req )

    def try_handle_view(self,req,ad,action,view):
        ## remove unicode shit
        view = str(view)
        uri = action._redirectUri
        if uri != None:
            req.headers_out[ "Location" ] = uri
            return 301 #apache.HTTP_MOVED_PERMANENTLY
        else:
    
            try:
                    vd = ad.views[ view ]
            except KeyError:
                return error.errorInvalidView(req,id,view)

            vname = vd.viewHandlerName
            vh = self.gview.getViewHandlerInstance( vname )
            vh.viewFile = vd.viewFile
            vh.context = self
            vh.action = action
            if not req.content_type:
                req.content_type = str(vd.viewContentType) #for unicode stuff
            vh.display( req )
                
                
        return 0 # apache.OK

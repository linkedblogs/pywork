# $Id: action.py,v 1.24 2004/06/24 08:56:41 julcicc Exp $
# vim:et:sts=4

import descriptor, util, exception, log, view
import xml.parsers.expat

try: from psyco.classes import *
except: pass

class ActionConfigurationError(exception.BaseException): pass

class ActionInstanceError(exception.BaseException): pass

"""
    The main action object. All application action objects *MUST* extend this class
    The client action object may receive http parameters by declaring writeable properties with the same name
"""

_globalSession = {}

class fileupload(object):
    def __init__(self, fname = None, data = None):
        self.filename = fname
        self.fileObject = data
    def __str__(self):
        return "fileupload(%s)" % self.filename

class Action(object):
    __slots__ = [ "_descriptor", "_redirectUri", "_session", "_name", "_locale", "_eventBindings", "_form", "_context", "_actionParameters", "_remoteAddress" ]

    def __init__(self):
        self._descriptor = None
        self._redirectUri = None

        #### ATTENTION _session not initialized here
        #### see descriptor.py (__actionCreator)
        if not hasattr(self,"_session"):
            self._session = None

        self._name = None
        self._locale = None
        self._eventBindings = {}
        self._form = {}
        self._variableFormProperties = False
        self._context = None
        self._actionParameters = {}
        self._eventParam = None
        self._remoteAddress = None

    def getCurrentEventParameter(self):
        return self._eventParam

    def setCurrentEventParamater(self,v):
        self._eventParam = v

    def getRemoteAddress(self):
        return self._remoteAddress

    def auth(self):
        return True

    def preExecute(self):
        return

    def execute(self):
        return None

    def postExecute(self):
        return

    def getSession(self):
        return self._session

    def getActionParameters(self):
        return self._actionParameters

    def bind(self,name,method):
        if name[ :1 ] != "_":
            raise ValueError, "Event bindings can be defined only on parameters beginning with an '_' character"
        else:
            if callable(method):
                self._eventBindings[ name ] = method
            else:
                raise TypeError, "Event bindings can only be made with callable objects, methods or functions"

    def unbind(self,name):
        self._eventBindings[ name ] = None

    def setVariableFormProperties(self,v):
        self._variableFormProperties = v

    def getRedirectUri(self):
        return self._redirectUri

    def setRedirectUri(self,v):
        self._redirectUri = v

"""
    An action map xml loaders, reads an XML and creates a dictionary with all the action descriptors found
    By now we do not perform any XML - validation! To be outside the classes
"""
class ActionMapXMLLoader(object):

    def __init__(self,configuration,availableViews = []):
        self.__parser = xml.parsers.expat.ParserCreate()
        self.__parser.StartElementHandler = self.start_element
        self.__parser.EndElementHandler = self.end_element
        self.__availableViews = availableViews
        self.__map = {}
        self.__names = []
        self.__conf = configuration
        self.__currentAction = None
        self.__aliases = {}
        self.__level = []


    def start_element(self, name, attrs):
        self.__level.append( name )

        ##convert attrs to str to eliminate unicode
        ##for k, v in attrs.items():
            ##attrs[ str(k) ] = str(v)

        if name == "action":
            self.__loadAction(attrs)
        elif name == "view":
            self.__loadView(attrs)
        elif name == "alias":
            self.__loadAlias(attrs)
        elif name == "include":
            self.__loadInclude(attrs)

    def end_element( self, name ):
        self.__level.remove( name )
        if name == "action":
            self.__map[ self.__currentAction.name ] = self.__currentAction
            self.__processAliases()


    def __processAliases(self):
        for alias in self.__aliases:
            self.__map[ alias ] = self.__currentAction

        self.__aliases = []

    def __loadAlias(self,attrs):
        name = self.__conf.replaceconf( attrs[ "name" ] )
        if name not in self.__names:
            self.__aliases.append( name )
            self.__names.append( name )
        else:
            raise ActionConfigurationError( "Action %s was defined more than once" % attrs[ "name" ] )

    def __loadAction(self, attrs):
        name = self.__conf.replaceconf( attrs[ "name" ] )
        if name not in self.__names:
            self.__aliases = []
            ad = descriptor.ActionDescriptor()
            ad.name = name
            ad.className = attrs[ "class" ]
            ad.moduleName = self.__conf.replaceconf( attrs["module" ] )
        
            # try to load class
            # it will throw an error if something's bad
            ad.loadClass()
            if not issubclass(ad.classObject,Action):
                raise TypeError( "%s is not subclass of Action" % ad.className )
            self.__currentAction = ad
            self.__names.append( self.__currentAction.name )
        else:
            raise ActionConfigurationError( "Action %s was defined more than once" % attrs[ "name" ] )
        
    def __loadView(self,attrs):

        if ( "view-handler" in attrs and attrs[ "view-handler" ] in self.__availableViews ) or "view-handler" not in attrs:

            vd = descriptor.ViewDescriptor()
            vd.name = attrs[ "name" ]

            vd.viewFile = self.__conf.replaceconf( attrs.get( "file" ) )

            if "view-handler" in attrs:
                vd.viewHandlerName = attrs[ "view-handler" ]
            else:
                vhn = view.resolve_viewfile( vd.viewFile )
                if vhn:
                    vd.viewHandlerName = vhn
                else:
                    vd.viewHandlerName = "PASS"

            if "content-type" in attrs: vd.viewContentType = attrs[ "content-type" ]

            if vd.name not in self.__currentAction.views:
                self.__currentAction.views[ vd.name ] = vd
            else:
                raise ActionConfigurationError( "View %s was definede more than once in action %s" % ( vd.name, self.__currentAction.name ) )
        else:
            raise ActionConfigurationError( "Unknown view-handler: %s for action %s" % ( attrs[ "view-handler"] , self.__currentAction.name ) )
        

    def __loadInclude(self, attrs):
        if self.__level[ -1: ][0] != "include":
            raise ActionConfigurationError( "Found include inside invalid element, element stack is: %s" % self.__level )
        
        if "file" in attrs:

            fileName = self.__conf.replaceconf( attrs[ "file" ] )
            a = ActionMapXMLLoader( self.__conf, self.__availableViews )
            a.parseFile( fileName )
            self.__map.update( a.map )

        else:
            raise ActionConfigurationError( "Found and include element without the file attribute" )
         
    def setAvailableViews(self,availableViews):
        self.__availableViews = availableViews
        
    def parseFile(self, fileName):
        self.__names = []
        f = file( fileName, "r" )
        self.__parser.ParseFile( f )
        f.close()

            
    def __getMap(self):
        return self.__map

    map = property(__getMap,doc="Returns a dictionary with all the configured action maps")


class ActionMap(object):
    
    def __init__(self):
        self.__map = {}
        self.__lastMtime = 0
        
    def loadFromFile(self, file, configuration, availableViews):
        path = util.searchpath( file )

        if path == "":
            raise ActionConfigurationError( file + " not found, try setting the PYWORK env var" )

        m = util.getlastmodified( path )

        # only load if something's changed
        if m != self.__lastMtime or configuration.reload == 2:
            a = ActionMapXMLLoader( configuration, availableViews )
            a.parseFile( path )
            self.__map = a.map
            self.__lastMtime = m


    def getActionDescriptor(self, actionId):

        try:
            return self.__map[actionId]
        except:
            log.logERROR( "Action %s not found on %s" % (actionId,str(self.__map)) )
            return None

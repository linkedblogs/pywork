# $Id: conf.py,v 1.15 2004/06/22 08:17:17 julcicc Exp $
# vim:et:sts=4

import re, log, util, xml, exception

try:
    from mod_python.Session import Session as DefaultSessionCall
except:
    def DefaultSessionCall( r ):
        return None

class ConfigurationError(exception.BaseException): pass

class ConfigurationXMLLoader(object):

    def __init__(self, conf):
        self.__parser = xml.parsers.expat.ParserCreate()
        self.__parser.StartElementHandler = self.start_element
        self.__map = {}
        self.__except = []
        self.__conf = conf

    def start_element(self, name, attrs):
        for k, v in attrs.items():
            attrs[ str(k) ] = str(v)

        if name == "global":

            if attrs.has_key( "name" ) and attrs.has_key( "value" ):
                name = attrs[ "name" ]
                value = self.__conf.replaceconf( attrs[ "value" ] )

                if name != "" and value != "":
                    self.__map[ name ] = value
        elif name == "except":
            if attrs.has_key( "class" ) and attrs.has_key( "action" ):
                klass = attrs[ "class" ]
                action = self.__conf.replaceconf( attrs[ "action" ] )

                if klass != "" and action != "":
                    self.__except.append( (klass,action) )

    def parseFile(self, fileName):
        f = file( fileName, "r" )
        self.__parser.ParseFile( f )
        f.close()

    def __getMap(self):
        return self.__map

    def __getExcept(self):
        return self.__except

    map = property(__getMap,doc="Returns a dictionary with all the configured global configurations")
    _except = property(__getExcept)
    
class Configuration(object):

    def __init__( self ):
        self.__regex = re.compile( "\${([^}]*)}" )
        self.__map = {}
        self.__lastmtime = 0
        self.reload = 1
        self.debug = 0
        self.timing = 0
        self.exceptionHandlers = {}
        self.sessionCall = DefaultSessionCall
        self.__options = {}

    def loadFromFile( self, file ):
        path = util.searchpath( file )

        if path == "":
            s = file + " not found, try setting the PYWORK env var"
            log.logERROR( s )
            raise ConfigurationError( s )
        else:
            m = util.getlastmodified( path )

            # only load if something's changed
            if m != self.__lastmtime or self.reload == 2:
                c = ConfigurationXMLLoader( self )
                c.parseFile( path )
                self.__map = c.map
                for t in c._except:
                    l = self.exceptionHandlers[ t[ 0 ] ] = t[ 1 ]
                    
                self.__lastmtime = m
                self.__initconf()

    
    def getconf( self, property, default = "" ):

        try:
            return self.__map[ str(property) ]
        except KeyError:
            try:
                #convert 2 str for apache table
                return self.__options[ str(property) ]
            except KeyError:
                return default

    def set_options(self, opts ):
        self.__options = opts

    def getconfint( self, property, default = 0 ):
        try:
            return int( self.getconf( property ) )
        except:
            return default

    def replaceconf( self, str ):
        if str:
            return self.__regex.sub( self.__doregmatch, str )
        else:
            return str

    def __initconf(self):
        self.reload= self.getconfint( "reload" )
        self.debug = self.getconfint( "debug" )
        self.timing = self.getconfint( "timing" )
        if self.getconf( "session" ):
            # expected in "module<.submodule>*.class" format 
            s = self.getconf( "session" )
            i = s.rfind( "." )
            if i >= 0:
                j = i + 1 
                self.sessionCall = util.resolveobject( s[:i], s[j:] )
            else: raise TypeError( "Invalid sesison class" )

    def __doregmatch(self, match ):
        s = match.group( 1 )
        conf = self.getconf( s )
        if conf == "":
            return s

        return conf

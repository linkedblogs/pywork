# Makefile for Python documentation
# ---------------------------------
PAPER=A4

# Ideally, you shouldn't need to edit beyond this point

INFODIR=	info
TOOLSDIR=	tools

# This is the *documentation* release, and is used to construct the file
# names of the downloadable tarballs.
RELEASE=0.4

PYTHON=	   python
DVIPS=	   dvips -N0 -t $(PAPER)

# This is ugly!  The issue here is that there are two different levels
# in the directory tree at which we execute mkhowto, so we can't
# define it just once using a relative path (at least not with the
# current implementation and Makefile structure).  We use the GNUish
# $(shell) function here to work around that restriction by
# identifying mkhowto and the commontex/ directory using absolute paths.
#
PWD=$(shell pwd)

# (The trailing colon in the value is needed; TeX places it's default
# set of paths at the location of the empty string in the path list.)
TEXINPUTS=$(PWD)/commontex:

# The mkhowto script can be run from the checkout using the first
# version of this variable definition, or from a preferred version
# using the second version.  The standard documentation is typically
# built using the second flavor, where the preferred version is from
# the Python CVS trunk.
#MKHOWTO=   TEXINPUTS=$(TEXINPUTS) $(PYTHON) $(PWD)/tools/mkhowto
MKHOWTO=   TEXINPUTS=$(TEXINPUTS) $(PWD)/tools/mkhowto

MKDVI=	   $(MKHOWTO) --paper=$(PAPER) --dvi
MKHTML=	   $(MKHOWTO) --html --about html/stdabout.dat \
		--iconserver ./icons --favicon ./icons/pyfav.gif \
		--address $(PYTHONDOCS) --up-link ./index.html \
		--up-title "PyWork Documentation Index" \
		--dvips-safe
MKISILOHTML=$(MKHOWTO) --html --about html/stdabout.dat \
		--iconserver ../icons \
		--l2h-init perl/isilo.perl --numeric --split 1 \
		--dvips-safe
MKISILO=   iSilo386 -U -y -rCR -d0
MKPDF=	   $(MKHOWTO) --paper=$(PAPER) --pdf
MKPS=	   $(MKHOWTO) --paper=$(PAPER) --ps

BUILDINDEX=$(TOOLSDIR)/buildindex.py

PYTHONDOCS="See <i><a href=\"about.html\">About this document...</a></i> for information on suggesting changes."
HTMLBASE=  file:`pwd`

# The emacs binary used to build the info docs. GNU Emacs 21 is required.
EMACS=     emacs

# The end of this should reflect the major/minor version numbers of
# the release:
WHATSNEW=whatsnew23

# what's what
MANDVIFILES=	paper-$(PAPER)/api.dvi paper-$(PAPER)/ext.dvi \
		paper-$(PAPER)/lib.dvi paper-$(PAPER)/mac.dvi \
		paper-$(PAPER)/ref.dvi paper-$(PAPER)/tut.dvi
HOWTODVIFILES=	paper-$(PAPER)/doc.dvi paper-$(PAPER)/inst.dvi \
		paper-$(PAPER)/dist.dvi paper-$(PAPER)/$(WHATSNEW).dvi

MANPDFFILES=	paper-$(PAPER)/api.pdf paper-$(PAPER)/ext.pdf \
		paper-$(PAPER)/lib.pdf paper-$(PAPER)/mac.pdf \
		paper-$(PAPER)/ref.pdf paper-$(PAPER)/tut.pdf
HOWTOPDFFILES=	paper-$(PAPER)/doc.pdf paper-$(PAPER)/inst.pdf \
		paper-$(PAPER)/dist.pdf paper-$(PAPER)/$(WHATSNEW).pdf

MANPSFILES=	paper-$(PAPER)/api.ps paper-$(PAPER)/ext.ps \
		paper-$(PAPER)/lib.ps paper-$(PAPER)/mac.ps \
		paper-$(PAPER)/ref.ps paper-$(PAPER)/tut.ps
HOWTOPSFILES=	paper-$(PAPER)/doc.ps paper-$(PAPER)/inst.ps \
		paper-$(PAPER)/dist.ps paper-$(PAPER)/$(WHATSNEW).ps

DVIFILES=	$(MANDVIFILES) $(HOWTODVIFILES)
PDFFILES=	$(MANPDFFILES) $(HOWTOPDFFILES)
PSFILES=	$(MANPSFILES) $(HOWTOPSFILES)

HTMLCSSFILES=html/api/api.css \
	html/doc/doc.css \
	html/ext/ext.css \
	html/lib/lib.css \
	html/mac/mac.css \
	html/ref/ref.css \
	html/tut/tut.css \
	html/inst/inst.css \
	html/dist/dist.css

ISILOCSSFILES=isilo/api/api.css \
	isilo/doc/doc.css \
	isilo/ext/ext.css \
	isilo/lib/lib.css \
	isilo/mac/mac.css \
	isilo/ref/ref.css \
	isilo/tut/tut.css \
	isilo/inst/inst.css \
	isilo/dist/dist.css

ALLCSSFILES=$(HTMLCSSFILES) $(ISILOCSSFILES)

INDEXFILES=html/api/api.html \
	html/doc/doc.html \
	html/ext/ext.html \
	html/lib/lib.html \
	html/mac/mac.html \
	html/ref/ref.html \
	html/tut/tut.html \
	html/inst/inst.html \
	html/dist/dist.html \
	html/whatsnew/$(WHATSNEW).html

ALLHTMLFILES=$(INDEXFILES) html/index.html html/modindex.html html/acks.html

COMMONPERL= perl/manual.perl perl/python.perl perl/l2hinit.perl

ANNOAPI=api/refcounts.dat tools/anno-api.py

include Makefile.deps

# These must be declared phony since there
# are directories with matching names:
.PHONY: api doc ext lib mac ref tut inst dist
.PHONY: html info isilo


# Main target
default:	pywork-html-$(RELEASE).tgz
all:		pywork-html-$(RELEASE).tgz	

dvi:	$(DVIFILES)
pdf:	$(PDFFILES)
ps:	$(PSFILES)

world:	ps pdf html distfiles


# Rules to build PostScript and PDF formats
.SUFFIXES: .dvi .ps

.dvi.ps:
	$(DVIPS) -o $@ $<


# Targets for each document:
# Python/C API Reference Manual
paper-$(PAPER)/api.dvi: $(ANNOAPIFILES)
	cd paper-$(PAPER) && $(MKDVI) api.tex

paper-$(PAPER)/api.pdf: $(ANNOAPIFILES)
	cd paper-$(PAPER) && $(MKPDF) api.tex

paper-$(PAPER)/api.tex: api/api.tex
	cp api/api.tex $@

paper-$(PAPER)/abstract.tex: api/abstract.tex $(ANNOAPI)
	$(PYTHON) $(TOOLSDIR)/anno-api.py -o $@ api/abstract.tex

paper-$(PAPER)/concrete.tex: api/concrete.tex $(ANNOAPI)
	$(PYTHON) $(TOOLSDIR)/anno-api.py -o $@ api/concrete.tex

paper-$(PAPER)/exceptions.tex: api/exceptions.tex $(ANNOAPI)
	$(PYTHON) $(TOOLSDIR)/anno-api.py -o $@ api/exceptions.tex

paper-$(PAPER)/init.tex: api/init.tex $(ANNOAPI)
	$(PYTHON) $(TOOLSDIR)/anno-api.py -o $@ api/init.tex

paper-$(PAPER)/intro.tex: api/intro.tex
	cp api/intro.tex $@

paper-$(PAPER)/memory.tex: api/memory.tex $(ANNOAPI)
	$(PYTHON) $(TOOLSDIR)/anno-api.py -o $@ api/memory.tex

paper-$(PAPER)/newtypes.tex: api/newtypes.tex $(ANNOAPI)
	$(PYTHON) $(TOOLSDIR)/anno-api.py -o $@ api/newtypes.tex

paper-$(PAPER)/refcounting.tex: api/refcounting.tex $(ANNOAPI)
	$(PYTHON) $(TOOLSDIR)/anno-api.py -o $@ api/refcounting.tex

paper-$(PAPER)/utilities.tex: api/utilities.tex $(ANNOAPI)
	$(PYTHON) $(TOOLSDIR)/anno-api.py -o $@ api/utilities.tex

paper-$(PAPER)/veryhigh.tex: api/veryhigh.tex $(ANNOAPI)
	$(PYTHON) $(TOOLSDIR)/anno-api.py -o $@ api/veryhigh.tex

# Distributing Python Modules
paper-$(PAPER)/dist.dvi: $(DISTFILES)
	cd paper-$(PAPER) && $(MKDVI) ../dist/dist.tex

paper-$(PAPER)/dist.pdf: $(DISTFILES)
	cd paper-$(PAPER) && $(MKPDF) ../dist/dist.tex

# Documenting Python
paper-$(PAPER)/doc.dvi: $(DOCFILES)
	cd paper-$(PAPER) && $(MKDVI) ../doc/doc.tex

paper-$(PAPER)/doc.pdf: $(DOCFILES)
	cd paper-$(PAPER) && $(MKPDF) ../doc/doc.tex

# Extending and Embedding the Python Interpreter
paper-$(PAPER)/ext.dvi: $(EXTFILES)
	cd paper-$(PAPER) && $(MKDVI) ../ext/ext.tex

paper-$(PAPER)/ext.pdf: $(EXTFILES)
	cd paper-$(PAPER) && $(MKPDF) ../ext/ext.tex

# Installing Python Modules
paper-$(PAPER)/inst.dvi: $(INSTFILES)
	cd paper-$(PAPER) && $(MKDVI) ../inst/inst.tex

paper-$(PAPER)/inst.pdf: $(INSTFILES)
	cd paper-$(PAPER) && $(MKPDF) ../inst/inst.tex

# Python Library Reference
paper-$(PAPER)/lib.dvi: $(LIBFILES)
	cd paper-$(PAPER) && $(MKDVI) ../lib/lib.tex

paper-$(PAPER)/lib.pdf: $(LIBFILES)
	cd paper-$(PAPER) && $(MKPDF) ../lib/lib.tex

# Macintosh Library Modules
paper-$(PAPER)/mac.dvi: $(MACFILES)
	cd paper-$(PAPER) && $(MKDVI) ../mac/mac.tex

paper-$(PAPER)/mac.pdf: $(MACFILES)
	cd paper-$(PAPER) && $(MKPDF) ../mac/mac.tex

# Python Reference Manual
paper-$(PAPER)/ref.dvi: $(REFFILES)
	cd paper-$(PAPER) && $(MKDVI) ../ref/ref.tex

paper-$(PAPER)/ref.pdf: $(REFFILES)
	cd paper-$(PAPER) && $(MKPDF) ../ref/ref.tex

# Python Tutorial
paper-$(PAPER)/tut.dvi: $(TUTFILES)
	cd paper-$(PAPER) && $(MKDVI) ../tut/tut.tex

paper-$(PAPER)/tut.pdf: $(TUTFILES)
	cd paper-$(PAPER) && $(MKPDF) ../tut/tut.tex

# What's New in Python X.Y
paper-$(PAPER)/$(WHATSNEW).dvi: whatsnew/$(WHATSNEW).tex
	cd paper-$(PAPER) && $(MKDVI) ../whatsnew/$(WHATSNEW).tex

paper-$(PAPER)/$(WHATSNEW).pdf: whatsnew/$(WHATSNEW).tex
	cd paper-$(PAPER) && $(MKPDF) ../whatsnew/$(WHATSNEW).tex

# The remaining part of the Makefile is concerned with various
# conversions, as described above.  See also the README file.

info:
	cd $(INFODIR) && $(MAKE) EMACS=$(EMACS) WHATSNEW=$(WHATSNEW)

# Targets to convert the manuals to HTML using Nikos Drakos' LaTeX to
# HTML converter.  For more info on this program, see
# <URL:http://cbl.leeds.ac.uk/nikos/tex2html/doc/latex2html/latex2html.html>.

# Note that LaTeX2HTML inserts references to an icons directory in
# each page that it generates.  I have placed a copy of this directory
# in the distribution to simplify the process of creating a
# self-contained HTML distribution; for this purpose I have also added
# a (trivial) index.html.  Change the definition of $ICONSERVER in
# perl/l2hinit.perl to use a different location for the icons directory.

# If you have the standard LaTeX2HTML icons installed, the versions shipped
# with this documentation should be stored in a separate directory and used
# instead.  The standard set does *not* include all the icons used in the
# Python documentation.

$(ALLCSSFILES): html/style.css
	cp $< $@

$(INDEXFILES): $(COMMONPERL) html/stdabout.dat tools/node2label.pl

html/acks.html: ACKS $(TOOLSDIR)/support.py $(TOOLSDIR)/mkackshtml
	$(PYTHON) $(TOOLSDIR)/mkackshtml --address $(PYTHONDOCS) \
		--favicon icons/pyfav.gif \
		--output html/acks.html <ACKS


# html/index.html is dependent on $(INDEXFILES) since we want the date
# on the front index to be updated whenever any of the child documents
# are updated and boilerplate.tex uses \today as the date.  The index
# files are not used to actually generate content.

BOILERPLATE=commontex/boilerplate.tex
html/index.html: $(INDEXFILES)
html/index.html: html/index.html.in $(BOILERPLATE) tools/rewrite.py
	$(PYTHON) tools/rewrite.py $(BOILERPLATE) \
		RELEASE=$(RELEASE) WHATSNEW=$(WHATSNEW) \
		<$< >$@

html/modindex.html: $(TOOLSDIR)/support.py $(TOOLSDIR)/mkmodindex
html/modindex.html: html/lib/lib.html html/mac/mac.html
	cd html && \
	 $(PYTHON) ../$(TOOLSDIR)/mkmodindex --columns 4 \
		--output modindex.html --address $(PYTHONDOCS) \
		--favicon icons/pyfav.gif \
		lib/modindex.html mac/modindex.html

html:	pyworkmanual

pyworkmanual: html/pyworkmanual.html
html/pyworkmanual.html: $(MANULFILES)
	$(MKHTML) --dir html/ --numeric --split 3 pyworkmanual/pyworkmanual.tex

HTMLPKGFILES=*.html *.css */*.gif

pywork-html-$(RELEASE).tar: html
	mkdir PyWork-Docs-$(RELEASE)
	cd html && tar cf ../temp.tar $(HTMLPKGFILES)
	cd PyWork-Docs-$(RELEASE) && tar xf ../temp.tar
	rm temp.tar
	tar cf pywork-html-$(RELEASE).tar PyWork-Docs-$(RELEASE)
	rm -r PyWork-Docs-$(RELEASE)

pywork-html-$(RELEASE).tgz:	pywork-html-$(RELEASE).tar
	gzip -9 <$? >$@
	rm -f pywork-html-$(RELEASE).tar

clean:
	rm -f pywork-html-$(RELEASE).tar pywork-html-$(RELEASE).tgz
	rm -Rf PyWork-Docs-$(RELEASE)
	rm -f pyworkmanual.* html/pyworkmanual.html html/node*html html/index.html html/*pl html/WARNINGS html/*how html/about.html


#!/usr/bin/env python

import sys
from distutils.core import setup, Extension

if sys.hexversion >= 33700000:

    setup(
       name = 'pywork',
       version = '0.4',
       author = 'Julian Ciccale',
       author_email = 'julcicc@users.sourceforge.net',
       maintainer = 'Julian Ciccale',
       maintainer_email = 'julcicc@users.sourceforge.net',
       url = 'http://pywork.sf.net',
       description ='A Python Web Application Framework',
       long_description ='A Python Web Application Framework implementing a Pull HMVC model, with ZPT and XSLT out of the box. Easy to configure and install. And even more easy to extend. It workd with mod_python and the Apache server to deliver high performance requests.',
       download_url = 'http://sourceforge.net/project/showfiles.php?group_id=96034',
       classifiers = [
              'Development Status :: 3 - Alpha',
              'Environment :: Web Environment',
              'Intended Audience :: Developers',
              'License :: OSI Approved :: Apache Software License',
              'Operating System :: MacOS :: MacOS X',
              'Operating System :: Microsoft :: Windows',
              'Operating System :: POSIX',
              'Programming Language :: Python',
              'Topic :: Software Development',
              ],
       extra_path = '',
       packages = ['pywork','contribpywork']
    )
else:

    setup(
       name = 'pywork',
       version = '0.4',
       description ='A Python Web Application Framework',
       extra_path = '',
       packages = ['pywork','contribpywork']
    )

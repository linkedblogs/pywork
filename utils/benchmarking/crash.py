import pywork.controller
import time

#import psyco

#psyco.log()
#psyco.profile()

class CommandLineRequest(object):
	
	def __init__(self):
		self.filename = ""
		self.headers_out = {}
		self.out = file( "/dev/null", "w" )
		self.parsed_uri = {}

	def write(self,s):
		self.out.write(s)


p = pywork.controller.Controller()
def callurl( fname ):
	req = CommandLineRequest()
	req.filename = fname
	req.parsed_uri[6]= fname
	req.parsed_uri[0]= fname
	p.processRequest( req )

def crash( fname, t ):
	print "Crash program for benchmarking pywork. 0.1 / 2003 / Julian Ciccale "
	print ""
	print "Run on %s " % time.strftime( "%a %d %m %H:%M:%S" )
	print ""
	i = 1
	n = t
	init = time.time()
	while i < n:
		callurl( fname )
		i = i + 1
	took = time.time() - init
	q = n / took
	print "%d requests (%s) in %f secs" % ( t, fname, took )
	print "Req/sec %f" % q

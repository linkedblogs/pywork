def foo():
	import crash
	#crash.crash( "/petstore/zptindex.action", 10000 )
	crash.crash( "/petstore/index.action", 10000 )

import profile
profile.run('foo()', 'fooprof')

import pstats
p = pstats.Stats('fooprof')
p.strip_dirs().sort_stats('time').print_stats()

#if __name__ == "__main__": foo()
